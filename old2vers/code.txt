import re

instruction_set = {
    'NOP': '00',
    'CLR A': 'E4',
    'DEC A': '14',
    'DEC R7': '1F',
    'MOV R0,#0x7F': '78',
    'MOV @R0,A': 'F6',
    'DJNZ R0,': 'D8',
    'MOV SP,#0x21': '75',
    'LJMP ': '02'
}

reverse_instruction_set = {
    '00': 'NOP',
    'E4': 'CLR A',
    '14': 'DEC A',
    '1F': 'DEC R7',
    '78': 'MOV R0,#',  # MOV R0,#0x7F
    'F6': 'MOV @R0,A',
    'D8': 'DJNZ R0,',  # DJNZ R0, label
    '75': 'MOV SP,#',  # MOV SP,#0x21
    '02': 'LJMP '  # LJMP label
}

def read_asm_code():
    asm_code = """
    CLR A
    DEC A
    DEC R7
    MOV R0,#0x7F
    MOV @R0,A
    DJNZ R0,LOOP
    MOV SP,#0x21
    LJMP END
    LOOP: CLR A
    END: DEC A
    """
    return asm_code.strip().split('\n')

def parse_asm_code(asm_lines):
    label_table = {}
    instructions = []
    current_address = 0

    for line in asm_lines:
        line = line.strip()
        if ':' in line:
            label, instruction = line.split(':')
            label = label.strip()
            instruction = instruction.strip()
            label_table[label] = current_address
        else:
            instruction = line

        if instruction:
            instructions.append((current_address, instruction))
            if instruction in instruction_set:
                current_address += len(instruction_set[instruction]) // 2
            elif instruction.startswith('MOV R0,#'):
                current_address += 2
            elif instruction.startswith('MOV SP,#'):
                current_address += 3
            elif instruction.startswith('DJNZ R0,') or instruction.startswith('LJMP '):
                current_address += 3
            else:
                current_address += 2

    return instructions, label_table

def asm_to_bin(instructions, label_table, instruction_set):
    bin_code = ""

    for address, instruction in instructions:
        if instruction in instruction_set:
            bin_code += instruction_set[instruction]
            if instruction.startswith('MOV R0,#'):
                bin_code += instruction.split('#')[-1].strip().replace('0x', '').upper()
            elif instruction.startswith('MOV SP,#'):
                bin_code += instruction.split('#')[-1].strip().replace('0x', '').upper()
        else:
            match = re.match(r'(DJNZ R0,|LJMP )(.*)', instruction)
            if match:
                instr_code = instruction_set[match.group(1)]
                label = match.group(2).strip()
                if label in label_table:
                    offset = label_table[label] - (address + 2)
                    if offset < 0:
                        offset = 256 + offset
                    bin_code += f'{instr_code}{offset:02X}'
                else:
                    raise ValueError(f"Label {label} is not defined.")
            else:
                raise ValueError(f"Instruction {instruction} is not supported.")

    return bin_code

def bin_to_asm(bin_code):
    asm_code = []
    i = 0
    while i < len(bin_code):
        byte = bin_code[i:i+2]
        if byte in reverse_instruction_set:
            instr = reverse_instruction_set[byte]
            if instr in ['MOV R0,#', 'DJNZ R0,', 'LJMP ', 'MOV SP,#']:
                param = bin_code[i+2:i+4]
                asm_code.append(f'{instr}0x{param}')
                i += 4
            else:
                asm_code.append(instr)
                i += 2
        else:
            asm_code.append(f'UNKNOWN ({byte})')
            i += 2
    return asm_code

def main():
    asm_lines = read_asm_code()
    instructions, label_table = parse_asm_code(asm_lines)
    try:
        bin_code = asm_to_bin(instructions, label_table, instruction_set)
        print("Binary Code:", bin_code)
        asm_code = bin_to_asm(bin_code)
        print("Assembler Code:")
        for line in asm_code:
            print(line)
    except ValueError as e:
        print(e)

if __name__ == "__main__":
    main()
